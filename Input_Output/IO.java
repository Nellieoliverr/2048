package Input_Output;

import java.io.*;

public class IO {
    private final String filePath = new File("").getAbsolutePath();
    private final String path_save = filePath + "/Input_Output/save.txt";
    private final String path_board = filePath + "/Input_Output/board.txt";

    public boolean writeBoard(int[][] board){

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path_board));

            for (int i = 0; i <= 3; i++) {
                StringBuilder line = new StringBuilder();
                for (int j = 0; j <= 3 ; j++) {
                    line.append(board[i][j]);
                    line.append(" ");
                }
                bufferedWriter.write(line.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public int[][] readBoard(){
        int[][] board = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};

        try {
            BufferedReader reader = new BufferedReader(new FileReader(path_board));
            String line = reader.readLine();
            int row_counter = 0;
            while(line != null){
                String[] row = line.split(" ");

                if(row.length == 4 && row_counter <= 3){
                    for (int i = 0; i <= 3; i++) {
                        board[row_counter][i] = Integer.parseInt(row[i]);
                    }
                }

                line = reader.readLine();
                row_counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
       return board;
    }

    public void writeScores(int current_score, int best_score){

        if(current_score > best_score){
            best_score = current_score;
        }

        String score = current_score + " " + best_score;
        try {

            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path_save));
            bufferedWriter.write(score);

            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int[] readScores(){
        String line = "0 0";

        try {
            BufferedReader reader = new BufferedReader(new FileReader(path_save));
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int[] score = new int[2];
        if(line != null) {
            String[] string = line.split(" ");
            score[0] = Integer.parseInt(string[0]);
            score[1] = Integer.parseInt(string[1]);
        }else{
            score[0] = 0;
            score[1] = 0;
        }
        return score;
    }

}
