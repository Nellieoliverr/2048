package GUI;

import javax.swing.*;
import java.awt.*;

public class Game extends JPanel {


    private JPanel panel_0_0;
    private JPanel panel_0_1;
    private JPanel panel_0_2;
    private JPanel panel_0_3;
    private JPanel panel_1_0;
    private JPanel panel_1_1;
    private JPanel panel_1_2;
    private JPanel panel_1_3;
    private JPanel panel_2_0;
    private JPanel panel_2_1;
    private JPanel panel_2_2;
    private JPanel panel_2_3;
    private JPanel panel_3_0;
    private JPanel panel_3_1;
    private JPanel panel_3_2;
    private JPanel panel_3_3;
    private JLabel number_0_0;

    private JPanel mainPanel;

    private JLabel number_0_1;
    private JLabel number_0_2;
    private JLabel number_0_3;
    private JLabel number_1_0;
    private JLabel number_2_0;
    private JLabel number_1_1;
    private JLabel number_1_2;
    private JLabel number_1_3;
    private JLabel number_2_1;
    private JLabel number_2_2;
    private JLabel number_2_3;
    private JLabel number_3_0;
    private JLabel number_3_1;
    private JLabel number_3_2;
    private JLabel number_3_3;

    private final Color blank = new Color(-1);
    private final Color colour1 = new Color(235, 182, 242);
    private final Color colour2 = new Color(216, 208, 247);
    private final Color colour3 = new Color(180, 191, 242);
    private final Color colour4 = new Color(145, 231, 228);
    private final Color colour5 = new Color(102, 220, 175);
    private final Color colour6 = new Color(127, 242, 110);
    private final Color colour7 = new Color(187, 247, 11);
    private final Color colour8 = new Color(237, 242, 113);
    private final Color colour9 = new Color(255, 231, 148);
    private final Color colour10 = new Color(247, 160, 117);
    private final Color colour11 = new Color(255, 89, 46);
    private final Color colour12 = new Color(247, 0, 9);

    public Game(int width, int height) {
        setUp(width, height);
    }

    private void setUp(int width, int height) {
        this.setVisible(true);
        this.setSize(width, height);
        this.add(mainPanel);
    }

    public void populate(int[][] board) {
        label_numbers(board);
        set_up_panel_colours(board);
    }

    private void label_numbers(int[][] board) {

        number_0_0.setText(" " + board[0][0] + " ");
        number_0_1.setText(" " + board[0][1] + " ");
        number_0_2.setText(" " + board[0][2] + " ");
        number_0_3.setText(" " + board[0][3] + " ");

        number_1_0.setText(" " + board[1][0] + " ");
        number_1_1.setText(" " + board[1][1] + " ");
        number_1_2.setText(" " + board[1][2] + " ");
        number_1_3.setText(" " + board[1][3] + " ");


        number_2_0.setText(" " + board[2][0] + " ");
        number_2_1.setText(" " + board[2][1] + " ");
        number_2_2.setText(" " + board[2][2] + " ");
        number_2_3.setText(" " + board[2][3] + " ");

        number_3_0.setText(" " + board[3][0] + " ");
        number_3_1.setText(" " + board[3][1] + " ");
        number_3_2.setText(" " + board[3][2] + " ");
        number_3_3.setText(" " + board[3][3] + " ");

    }

    private void set_up_panel_colours(int[][] board) {

        panel_0_0.setBackground(pickPanelColor(board[0][0]));
        panel_0_1.setBackground(pickPanelColor(board[0][1]));
        panel_0_2.setBackground(pickPanelColor(board[0][2]));
        panel_0_3.setBackground(pickPanelColor(board[0][3]));

        panel_1_0.setBackground(pickPanelColor(board[1][0]));
        panel_1_1.setBackground(pickPanelColor(board[1][1]));
        panel_1_2.setBackground(pickPanelColor(board[1][2]));
        panel_1_3.setBackground(pickPanelColor(board[1][3]));

        panel_2_0.setBackground(pickPanelColor(board[2][0]));
        panel_2_1.setBackground(pickPanelColor(board[2][1]));
        panel_2_2.setBackground(pickPanelColor(board[2][2]));
        panel_2_3.setBackground(pickPanelColor(board[2][3]));

        panel_3_0.setBackground(pickPanelColor(board[3][0]));
        panel_3_1.setBackground(pickPanelColor(board[3][1]));
        panel_3_2.setBackground(pickPanelColor(board[3][2]));
        panel_3_3.setBackground(pickPanelColor(board[3][3]));
    }

    private Color pickPanelColor(int value) {
        Color panel_colour = blank;
        if (value == 1) {
            panel_colour = colour1;
        } else if (value == 2) {
            panel_colour = colour2;
        } else if (value == 4) {
            panel_colour = colour3;
        } else if (value == 8) {
            panel_colour = colour4;
        } else if (value == 16) {
            panel_colour = colour5;
        } else if (value == 32) {
            panel_colour = colour6;
        }else if (value == 64) {
            panel_colour = colour7;
        }else if (value ==128) {
            panel_colour = colour8;
        }else if (value ==256) {
            panel_colour = colour9;
        }else if (value ==512) {
            panel_colour = colour10;
        }else if (value == 1024) {
            panel_colour = colour11;
        }else if (value == 2048) {
            panel_colour = colour12;
        }else if (value > 2048) {
            panel_colour = pickPanelColor(value / 2);
        }
        return panel_colour;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.setBackground(new Color(-1114881));
        mainPanel.setForeground(new Color(-264222));
        panel_0_0 = new JPanel();
        panel_0_0.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_0_0.setBackground(new Color(-1));
        mainPanel.add(panel_0_0, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        panel_0_0.setBorder(BorderFactory.createTitledBorder(""));
        number_0_0 = new JLabel();
        number_0_0.setEnabled(false);
        Font number_0_0Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_0_0.getFont());
        if (number_0_0Font != null) number_0_0.setFont(number_0_0Font);
        number_0_0.setForeground(new Color(-16777216));
        number_0_0.setText("Label");
        panel_0_0.add(number_0_0, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel_0_3 = new JPanel();
        panel_0_3.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_0_3.setBackground(new Color(-1));
        mainPanel.add(panel_0_3, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_0_3 = new JLabel();
        number_0_3.setEnabled(false);
        Font number_0_3Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_0_3.getFont());
        if (number_0_3Font != null) number_0_3.setFont(number_0_3Font);
        number_0_3.setForeground(new Color(-16777216));
        number_0_3.setText("Label");
        panel_0_3.add(number_0_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_0_1 = new JPanel();
        panel_0_1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_0_1.setBackground(new Color(-1));
        mainPanel.add(panel_0_1, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_0_1 = new JLabel();
        number_0_1.setEnabled(false);
        Font number_0_1Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_0_1.getFont());
        if (number_0_1Font != null) number_0_1.setFont(number_0_1Font);
        number_0_1.setForeground(new Color(-16777216));
        number_0_1.setText("Label");
        panel_0_1.add(number_0_1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_0_2 = new JPanel();
        panel_0_2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_0_2.setBackground(new Color(-1));
        mainPanel.add(panel_0_2, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_0_2 = new JLabel();
        number_0_2.setEnabled(false);
        Font number_0_2Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_0_2.getFont());
        if (number_0_2Font != null) number_0_2.setFont(number_0_2Font);
        number_0_2.setForeground(new Color(-16777216));
        number_0_2.setText("Label");
        panel_0_2.add(number_0_2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_3_3 = new JPanel();
        panel_3_3.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_3_3.setBackground(new Color(-1));
        mainPanel.add(panel_3_3, new com.intellij.uiDesigner.core.GridConstraints(3, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_3_3 = new JLabel();
        number_3_3.setEnabled(false);
        Font number_3_3Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_3_3.getFont());
        if (number_3_3Font != null) number_3_3.setFont(number_3_3Font);
        number_3_3.setForeground(new Color(-16777216));
        number_3_3.setText("Label");
        panel_3_3.add(number_3_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_1_3 = new JPanel();
        panel_1_3.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_1_3.setBackground(new Color(-1));
        mainPanel.add(panel_1_3, new com.intellij.uiDesigner.core.GridConstraints(1, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_1_3 = new JLabel();
        number_1_3.setEnabled(false);
        Font number_1_3Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_1_3.getFont());
        if (number_1_3Font != null) number_1_3.setFont(number_1_3Font);
        number_1_3.setForeground(new Color(-16777216));
        number_1_3.setText("Label");
        panel_1_3.add(number_1_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_2_3 = new JPanel();
        panel_2_3.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_2_3.setBackground(new Color(-1));
        mainPanel.add(panel_2_3, new com.intellij.uiDesigner.core.GridConstraints(2, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_2_3 = new JLabel();
        number_2_3.setEnabled(false);
        Font number_2_3Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_2_3.getFont());
        if (number_2_3Font != null) number_2_3.setFont(number_2_3Font);
        number_2_3.setForeground(new Color(-16777216));
        number_2_3.setText("Label");
        panel_2_3.add(number_2_3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_1_2 = new JPanel();
        panel_1_2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_1_2.setBackground(new Color(-1));
        mainPanel.add(panel_1_2, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_1_2 = new JLabel();
        number_1_2.setEnabled(false);
        Font number_1_2Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_1_2.getFont());
        if (number_1_2Font != null) number_1_2.setFont(number_1_2Font);
        number_1_2.setForeground(new Color(-16777216));
        number_1_2.setText("Label");
        panel_1_2.add(number_1_2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_3_2 = new JPanel();
        panel_3_2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_3_2.setBackground(new Color(-1));
        mainPanel.add(panel_3_2, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_3_2 = new JLabel();
        number_3_2.setEnabled(false);
        Font number_3_2Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_3_2.getFont());
        if (number_3_2Font != null) number_3_2.setFont(number_3_2Font);
        number_3_2.setForeground(new Color(-16777216));
        number_3_2.setText("Label");
        panel_3_2.add(number_3_2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_2_2 = new JPanel();
        panel_2_2.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_2_2.setBackground(new Color(-1));
        mainPanel.add(panel_2_2, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_2_2 = new JLabel();
        number_2_2.setEnabled(false);
        Font number_2_2Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_2_2.getFont());
        if (number_2_2Font != null) number_2_2.setFont(number_2_2Font);
        number_2_2.setForeground(new Color(-16777216));
        number_2_2.setText("Label");
        panel_2_2.add(number_2_2, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_1_1 = new JPanel();
        panel_1_1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_1_1.setBackground(new Color(-1));
        mainPanel.add(panel_1_1, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_1_1 = new JLabel();
        number_1_1.setEnabled(false);
        Font number_1_1Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_1_1.getFont());
        if (number_1_1Font != null) number_1_1.setFont(number_1_1Font);
        number_1_1.setForeground(new Color(-16777216));
        number_1_1.setText("Label");
        panel_1_1.add(number_1_1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_1_0 = new JPanel();
        panel_1_0.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_1_0.setBackground(new Color(-1));
        mainPanel.add(panel_1_0, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_1_0 = new JLabel();
        number_1_0.setEnabled(false);
        Font number_1_0Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_1_0.getFont());
        if (number_1_0Font != null) number_1_0.setFont(number_1_0Font);
        number_1_0.setForeground(new Color(-16777216));
        number_1_0.setText("Label");
        panel_1_0.add(number_1_0, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_3_1 = new JPanel();
        panel_3_1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_3_1.setBackground(new Color(-1));
        mainPanel.add(panel_3_1, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_3_1 = new JLabel();
        number_3_1.setEnabled(false);
        Font number_3_1Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_3_1.getFont());
        if (number_3_1Font != null) number_3_1.setFont(number_3_1Font);
        number_3_1.setForeground(new Color(-16777216));
        number_3_1.setText("Label");
        panel_3_1.add(number_3_1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_3_0 = new JPanel();
        panel_3_0.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_3_0.setBackground(new Color(-1));
        mainPanel.add(panel_3_0, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_3_0 = new JLabel();
        number_3_0.setEnabled(false);
        Font number_3_0Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_3_0.getFont());
        if (number_3_0Font != null) number_3_0.setFont(number_3_0Font);
        number_3_0.setForeground(new Color(-16777216));
        number_3_0.setText("Label");
        panel_3_0.add(number_3_0, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_2_1 = new JPanel();
        panel_2_1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_2_1.setBackground(new Color(-1));
        mainPanel.add(panel_2_1, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        number_2_1 = new JLabel();
        number_2_1.setEnabled(false);
        Font number_2_1Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_2_1.getFont());
        if (number_2_1Font != null) number_2_1.setFont(number_2_1Font);
        number_2_1.setForeground(new Color(-16777216));
        number_2_1.setText("Label");
        panel_2_1.add(number_2_1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        panel_2_0 = new JPanel();
        panel_2_0.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel_2_0.setBackground(new Color(-1));
        panel_2_0.setEnabled(true);
        panel_2_0.setOpaque(true);
        panel_2_0.setRequestFocusEnabled(true);
        panel_2_0.setVerifyInputWhenFocusTarget(true);
        panel_2_0.setVisible(true);
        mainPanel.add(panel_2_0, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(90, 80), null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setAutoscrolls(false);
        panel1.setBackground(new Color(-1));
        panel1.setEnabled(true);
        panel1.setOpaque(false);
        panel_2_0.add(panel1, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(100, 75), null, 0, false));
        number_2_0 = new JLabel();
        number_2_0.setEnabled(false);
        Font number_2_0Font = this.$$$getFont$$$("Malayalam Sangam MN", Font.PLAIN, 26, number_2_0.getFont());
        if (number_2_0Font != null) number_2_0.setFont(number_2_0Font);
        number_2_0.setForeground(new Color(-16777216));
        number_2_0.setText("Label");
        panel1.add(number_2_0, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }
}
