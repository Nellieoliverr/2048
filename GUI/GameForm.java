package GUI;

import Controls.Board_Mechanics;
import Controls.Controller;
import Input_Output.IO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

enum move{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public class GameForm extends JFrame implements KeyListener {
    private JPanel mainPanel;
    private JLabel score_input;
    private JLabel Score;
    private JButton Exit;
    private JPanel OptionPanel;
    private JPanel GamePanel;
    private JButton Menu_Button;

    private int top_score, current_score;
    private Controller controller;
    private int keyID;
    private Board_Mechanics board_mechanics;
    private Game tiles_2048;

    public GameForm(int top_score, int current_score, Controller.gameType gameType) {
        this.top_score = top_score;
        this.current_score = current_score;

        this.add(mainPanel);
        this.setSize(400, 400);
        this.addKeyListener(this);
        this.setFocusable(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //set up score
        updateScore(current_score);

        tiles_2048 = new Game(GamePanel.getWidth(), GamePanel.getHeight());

        tiles_2048.setVisible(true);
        tiles_2048.setFocusable(true);
        GamePanel.add(tiles_2048);
        GamePanel.setLayout(new GridLayout(0, 1));

        init_game(gameType);

        //action listener
        Exit.addActionListener(e -> {
            generatePopUpForSave();
            exit();
        });

        Menu_Button.addActionListener(e -> {
            generatePopUpForSave();
            go_to_menu();
        });
    }
    //-----------------------score controls-------------------------

    private void updateScore(int score) {
        current_score = score;
        score_input.setText(score + "");
    }

    private void updateTopScore() {
        if (current_score > top_score) {
            this.top_score = current_score;
        }
    }
    //-----------------------end of score controls-------------------------
    //-----------------------game controls-------------------------

    private void init_game(Controller.gameType game_type) {
        //init
        controller = new Controller(game_type);

        board_mechanics = controller.board_mechanics;
        board_mechanics.setUpAvailability();

        //get board ready
        board_mechanics.generateNewNumber();
        tiles_2048.populate(board_mechanics.getBoard());
    }


    private void stop_game(Controller controller) {
        controller.saveBoard();
    }


    private void update_game(Board_Mechanics.move next_move) {
        if (next_move != null) {
            //update the scores
            updateScore(board_mechanics.getScore());
            updateTopScore();

            board_mechanics.updateBoard(next_move);

            if (!board_mechanics.checkNextMoves()) {
                loose_game();
                return;
            } else if (board_mechanics.checkFor2048()&& !board_mechanics.won()) {
                generatePopUpForGameCompletion();
            }
            //----- if the game has possible moves
            board_mechanics.generateNewNumber();

            //display changes
            tiles_2048.populate(board_mechanics.getBoard());

        }

    }

    private void exit() {
        this.setVisible(false);
        this.dispose();
        try {
            throw new gameStopException("Exit 0");

        } catch (gameStopException e) {
            stop_game(controller);
        }
    }

    private void go_to_menu() {
        this.setVisible(false);
        startMenu new_frame = new startMenu();
        new_frame.setVisible(true);
        this.dispose();
        try {
            throw new gameStopException("Exit 0");

        } catch (gameStopException e) {
            stop_game(controller);
        }
    }
    //-----------------------end of game controls-------------------------


    //----------------------- pop up menus-----------------------
    private void loose_game() {

        int continue_game = JOptionPane.showConfirmDialog(null,
                "Score: " + this.current_score + "\n" + "Unfortunately you didn't reach 2048 on your board" + "\nWould you like to play again?", "You lost at : " + this.current_score, JOptionPane.YES_NO_OPTION);

        if (continue_game == JOptionPane.YES_OPTION) {//yes
            init_game(Controller.gameType.NEW);
        } else {
            go_to_menu();
        }

        stop_game(controller);

    }


    private void generatePopUpForSave() {
        int save = JOptionPane.showConfirmDialog(null,
                "Do you want to save your progress?", "Save Options", JOptionPane.YES_NO_OPTION);

        if (save == JOptionPane.YES_OPTION) {//yes
            IO io = new IO();
            io.writeScores(current_score, top_score);
        }
    }


    private void generatePopUpForGameCompletion() {
        int complete = JOptionPane.showConfirmDialog(null,
                "Well done!\n You've managed to get the 2048 tile.\n Are you wanting to continue playing?", "2048 Achieved", JOptionPane.YES_NO_OPTION);

        IO io = new IO();
        io.writeScores(current_score, top_score);

        if (complete == JOptionPane.NO_OPTION) {
            go_to_menu();
        }
    }

    //----------------------- end of pop up menus -----------------------


    //private Board_Mechanics.move move;

    //----------------------- keyboard input -----------------------
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        keyID = e.getKeyCode();
        update_game(executeKey());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keyID = 0;
    }

    private Board_Mechanics.move executeKey() {

        if (keyID == KeyEvent.VK_W | keyID == KeyEvent.VK_UP) {

            return Board_Mechanics.move.UP;
        } else if (keyID == KeyEvent.VK_D | keyID == KeyEvent.VK_RIGHT) {

            return Board_Mechanics.move.RIGHT;
        } else if (keyID == KeyEvent.VK_S | keyID == KeyEvent.VK_DOWN) {

            return Board_Mechanics.move.DOWN;
        } else if (keyID == KeyEvent.VK_A | keyID == KeyEvent.VK_LEFT) {

            return Board_Mechanics.move.LEFT;
        }
        return null;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.setEnabled(false);
        OptionPanel = new JPanel();
        OptionPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 6, new Insets(0, 0, 0, 0), -1, -1));
        OptionPanel.setBackground(new Color(-8216899));
        mainPanel.add(OptionPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_NORTH, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        Score = new JLabel();
        Font ScoreFont = this.$$$getFont$$$("Chalkduster", Font.PLAIN, 18, Score.getFont());
        if (ScoreFont != null) Score.setFont(ScoreFont);
        Score.setForeground(new Color(-1));
        Score.setText("Score:");
        OptionPanel.add(Score, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        score_input = new JLabel();
        Font score_inputFont = this.$$$getFont$$$("Chalkduster", Font.ITALIC, 18, score_input.getFont());
        if (score_inputFont != null) score_input.setFont(score_inputFont);
        score_input.setForeground(new Color(-1));
        score_input.setText("input");
        OptionPanel.add(score_input, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Exit = new JButton();
        Exit.setBackground(new Color(-8405830));
        Exit.setEnabled(true);
        Font ExitFont = this.$$$getFont$$$("Myanmar MN", Font.PLAIN, 18, Exit.getFont());
        if (ExitFont != null) Exit.setFont(ExitFont);
        Exit.setForeground(new Color(-16777216));
        Exit.setHideActionText(false);
        Exit.setText("Exit");
        OptionPanel.add(Exit, new com.intellij.uiDesigner.core.GridConstraints(0, 5, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Menu_Button = new JButton();
        Menu_Button.setBackground(new Color(-8405830));
        Menu_Button.setEnabled(true);
        Font Menu_ButtonFont = this.$$$getFont$$$("Myanmar MN", Font.PLAIN, 18, Menu_Button.getFont());
        if (Menu_ButtonFont != null) Menu_Button.setFont(Menu_ButtonFont);
        Menu_Button.setForeground(new Color(-16777216));
        Menu_Button.setHideActionText(false);
        Menu_Button.setText("Menu");
        OptionPanel.add(Menu_Button, new com.intellij.uiDesigner.core.GridConstraints(0, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));final com.intellij.uiDesigner.core.Spacer spacer1 = new com.intellij.uiDesigner.core.Spacer();
        OptionPanel.add(spacer1, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        GamePanel = new JPanel();
        GamePanel.setLayout(new CardLayout(170, 170));
        GamePanel.setBackground(new Color(-1));
        GamePanel.setForeground(new Color(-1114881));
        mainPanel.add(GamePanel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

    class gameStopException extends Exception {
        gameStopException(String message) {
            super(message);
        }
    }

    //---------- autogen stuff


}
