package Move_Tiles;

public class Right extends Move_Tiles {

    @Override
    public void push(int[][] board_new){
        for (int row = 3; row >= 1 ; row--) {
            for(int column = 0; column <= 3; column++){
                if(board_new[column][row]== 0 && board_new[column][row-1]!= 0){
                    int[] loc1 = {column,row};
                    int[] loc2 = {column, row-1};
                    swap(board_new,loc1,loc2);
                }
            }
        }
    }

    public void calc(int column, int row, int[][] board_new){
        int val1 = board_new[column][row];
        int val2 =  board_new[column][row+1];
        if (val1 != 0) {
            if (column <3) {
                if (board_new[column][row] == board_new[column][row+1]) {
                    score = score + board[column][row] * 2;
                    board_new[column][row] = board[column][row] * 2;
                    board_new[column][row+1] = 0;
                }
            }
        }else{
            board_new[column][row] = 0;
        }
    }

    public void move(){
        int[][] board_new = board;

        push(board_new);

        for(int col = 0; col <= 3; col++) {
            for (int row = 0; row < 3; row++) {
                calc(col,row,board_new);
            }
            push(board_new);
        }
        board = board_new;
    }

}
