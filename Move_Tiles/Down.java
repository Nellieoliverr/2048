package Move_Tiles;

public class Down extends Move_Tiles{
    @Override
    public void push(int[][] board_new){
        for (int row = 0; row <= 3 ; row++) {
            for(int column = 3; column > 0; column--){
                if(board_new[column][row]== 0 && board_new[column-1][row]!= 0){
                    int[] loc1 = {column,row};
                    int[] loc2 = {column-1, row};
                    swap(board_new,loc1,loc2);
                }
            }
        }


    }

    public void calc(int column, int row, int[][] board_new){
        if (board_new[column][row] != 0) {
            if (column >= 1) {
                int next_col_item = column - 1;

                if (board_new[column][row] == board_new[next_col_item][row]) {
                    score = score + board[column][row] * 2;
                    board_new[column][row] = board[column][row] * 2;
                    board_new[next_col_item][row] = 0;
                }
            }
        }else{
            board_new[column][row] = 0;
        }
    }

    public void move(){

        int[][] board_new = board;

        push(board_new);

        for(int row = 0; row <= 3; row++) {
            for (int column = 3; column >= 1; column--) {
                push(board_new);
                calc(column,row,board_new);
            }
        }

        board = board_new;
    }
}
