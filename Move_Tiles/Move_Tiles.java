package Move_Tiles;

public abstract class Move_Tiles {

    protected int[][] board;
    protected int score;

    public void setUp(int[][] current_board, int old_score){
        score = old_score;
        if(current_board!= null) {
            board = current_board;
        }
    }

    public abstract void calc(int column, int row, int[][] board_new);

    public abstract void push(int[][] board_new);

    public abstract void move();

    public void swap(int[][] board,int[] loc1, int[] loc2 ){
        int temp = board[loc1[0]][loc1[1]];
        board[loc1[0]][loc1[1]] = board[loc2[0]][loc2[1]];
        board[loc2[0]][loc2[1]] = temp;
    }

    public int[][] getBoard() {
        return board;
    }
    public int getScore() { return score;}
}
