package Move_Tiles;

public class Up extends Move_Tiles{

    @Override
    public void calc(int column, int row, int[][] board_new) {
        if (board_new[column][row] != 0) {
            if (column <= 2) {
                int next_col_item = column + 1;

                if (board_new[column][row] == board_new[next_col_item][row]) {
                    board_new[column][row] = board[column][row] * 2;
                    board_new[next_col_item][row] = 0;
                }
            }

            if (board_new[column][row] == -1) {
                board_new[column][row] = board[column][row];
            }
        }else{
            board_new[column][row] = 0;
        }
    }

    @Override
    public void push(int[][] board_new) {
        for(int row = 0; row <=3; row++){
            for(int column = 0; column <= 2; column++){
                if(board_new[column][row]== 0 && board_new[column+1][row]!= 0){
                    score = score + board[column][row] * 2;
                    int[] loc1 = {column,row};
                    int[] loc2 = {column+1, row};
                    swap(board_new,loc1,loc2);
                }
            }
        }
    }

    @Override
    public void move() {
        int[][] board_new = board;

        push(board_new);

        //do the calculations
        for(int row = 0; row <=3; row++){
            for(int column = 0; column <= 3; column++){
                push(board_new);
                calc(column,row,board_new);
            }
        }

        board = board_new;
    }
}
