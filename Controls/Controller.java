package Controls;

import Input_Output.IO;

import java.util.ArrayList;

import static Controls.Controller.gameType.*;

public class Controller{
    private int[][] board;
    private final IO save;
    public Board_Mechanics board_mechanics;

    public enum gameType{
        CONTINUE,
        NEW
    }


    public Controller(gameType type){
        save = new IO();
        board = new int[4][4];
        if(type.equals(NEW)){
            setUpNewBoard();
        }else{
            loadBoard();
        }
    }

    private void loadBoard(){//<-------------------------------------------------------------
        board = save.readBoard();
        ArrayList<String> availability = new ArrayList<>();
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                if(board[row][col] == 0){
                    availability.add(row + "" + col);
                }
            }
        }

        int scores[] = save.readScores();

        board_mechanics = new Board_Mechanics(board,scores[0]);
        board_mechanics.setFree_spaces(availability);
    }

    public Boolean saveBoard(){

        return save.writeBoard(board_mechanics.getBoard());
    }


    private void setUpNewBoard(){
        ArrayList<String> availability = new ArrayList<>();
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                board[row][col] = 0;
                availability.add(row + "" + col );
            }
        }
        board_mechanics = new Board_Mechanics(board,0);
        board_mechanics.setFree_spaces(availability);
    }

}
