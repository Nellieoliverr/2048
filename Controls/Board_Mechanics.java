package Controls;

import Move_Tiles.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Board_Mechanics {
    private int[][] board;
    private ArrayList<String> free_spaces;
    private int score;
    private boolean won = false;
    public enum move{
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public Board_Mechanics(int[][] current_board, int input_score){
        score = input_score;
        free_spaces = new ArrayList<>();
        if(current_board!= null) {
            board = current_board;
        }
    }

    private void move_tile( Move_Tiles tile){
        tile.setUp(board, score);
        tile.move();
        board = tile.getBoard();
        score = tile.getScore();
    }

    public void updateBoard(move gameMove){
        Move_Tiles tile;
        switch (gameMove){
            case UP:
                tile = new Up();
                move_tile(tile);
                break;
            case DOWN:
                tile = new Down();
                move_tile(tile);
                break;
            case LEFT:
                tile = new Left();
                move_tile(tile);
                break;
            case RIGHT:
                tile = new Right();
                move_tile(tile);
                break;
        }
        updateAvailability();
    }

    /**
     * @return true if more moves is possible. Otherwise false.
     */
    public boolean checkNextMoves(){
        //if board is not full, no point checking moves
        if(!free_spaces.isEmpty()){
            return true;
        }

        int[][] future_board = board;
        Move_Tiles tile = new Right();

        for (int i = 0; i < 4; i++) {

            switch (i){
                case 1:
                    tile = new Left();
                case 2 :
                    tile = new Up();
                case 3:
                    tile = new Down();
            }

            tile.setUp(future_board,0);
            tile.move();
            future_board = tile.getBoard();

            if(!Arrays.deepEquals(future_board, board)){
                return true;
            }
        }
        return false;

    }

    public void setUpAvailability(){
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                    free_spaces.add(row +  "" +  col);
            }
        }

    }


    private void updateAvailability(){
        free_spaces.clear();
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                if(board[row][col] == 0){
                    free_spaces.add(row +  "" +  col);
                }
            }
        }
    }

    public boolean checkFor2048(){
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                if(board[row][col] == 2048){
                    won = true;
                    return true;
                }
            }
        }
        return false;
    }
    private int[] selectFreeCell(Random rand){

        int[] cell = new int[2];
        int random = rand.nextInt(free_spaces.size());
        String random_location = free_spaces.get(random);
        free_spaces.remove(random);

        cell[0] = Integer.parseInt(Character.toString(random_location.charAt(0)));
        cell[1] = Integer.parseInt(Character.toString(random_location.charAt(1)));
        return cell;

    }

    public void printBoard(int[][] board) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(board[i][j] + "");
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.print("\n\n");
    }


    public void generateNewNumber(){
        Random rand = new Random(System.currentTimeMillis());
        int location[] = selectFreeCell(rand);
        int numbers[] = {1,2,4};
        int new_number = numbers[rand.nextInt(2)];
        board[location[0]][location[1]] = new_number;
    }

    //---boring code----//
    public int[][] getBoard() {
        return board;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }

    public ArrayList<String> getFree_spaces() {
        return free_spaces;
    }

    public void setFree_spaces(ArrayList<String> free_spaces) {
        this.free_spaces = free_spaces;
    }

    public int getScore() {
        return score;
    }

    public boolean won(){
        return won;
    }
}
